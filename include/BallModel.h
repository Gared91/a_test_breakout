#ifndef BALLMODEL_H
#define BALLMODEL_H

#include <Model.h>
#include <vector>

class BallModel : public Model {
private:
    float sectorCount;
    float stackCount;
    float radius;
    std::vector<Vertex> verticesVector;
    std::vector<unsigned int> indicesVector;

    glm::vec2 velocity;

public:
    BallModel();
    ~BallModel();

    void update(float deltaTime);
    void setVelocity(float x, float y);
    glm::vec2 getVelocity();

    void getVertices(Vertex* vertices) override;
    void getIndices(unsigned int* indices) override;
};

#endif // BALLMODEL_H
