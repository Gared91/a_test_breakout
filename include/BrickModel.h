#ifndef BRICKMODEL_H
#define BRICKMODEL_H

#include <Model.h>


class BrickModel : public Model {
public:
    BrickModel();
    ~BrickModel();

    void getVertices(Vertex* vertices) override;
    void getIndices(unsigned int* indices) override;
};

#endif // BRICKMODEL_H
