#ifndef CAMERA_H
#define CAMERA_H

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

class Camera {
private:
    glm::vec3 position;
    glm::vec3 origin;

public:
    Camera();
    ~Camera();

    void setPosition(float x, float y);
    void move(float x, float y);

    void getPVMMatrix(float* worldMatrix, float* viewMatrix, float* projectionMatrix);
};

#endif // CAMERA_H
