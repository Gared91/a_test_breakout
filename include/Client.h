#ifndef CLIENT_H
#define CLIENT_H

#include "Thread.h"
#include "Event.h"
#include <functional>
#include <string>
#include <queue>
#include "Lock.h"

class ClientSendThread: public Thread {
    int iResult;
    std::queue<std::string*> dataToSend;
    Lock lock;
public:
    SOCKET socket;

    void sendData(std::string* data);
    void* run() override;
};

class Client {
    WSADATA wsaData;
    SOCKET connectSocket;
    int iResult;
    ClientSendThread* sendThread;

public:
    std::function<void(Event,std::string*)> clientCallback;

    Client();
    virtual ~Client();
    void start();
    void sendData(std::string* data);
};

#endif // CLIENT_H
