#ifndef CLIENTTHREAD_H
#define CLIENTTHREAD_H

#include <Thread.h>
#include <Client.h>



class ClientThread: public Thread {
    Client* client;
public:
	ClientThread(Client* CLIENT);
	void* run() override;
};

#endif // CLIENTTHREAD_H
