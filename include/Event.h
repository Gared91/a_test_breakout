#ifndef EVENT_H
#define EVENT_H

enum Event {
    ClientStartError,
    ClientStarted,
    ClientData,
    ServerStartError,
    ServerStarted,
    ServerNewClient,
    ServerData,

    //Commands
    CmdBallMove,// int(cmd),float(posX),float(posY)
    CmdPlayerMove,// int(cmd),float(posX),float(posY)
    CmdHideBrick,// int(cmd),int(index)

    CmdHitFlor,// int(cmd)
};

#endif // EVENT_H
