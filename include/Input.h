#ifndef INPUT_H
#define INPUT_H

class Input {
public:
    Input();
    ~Input();

	void clear();

	void keyDown(unsigned int);
	void keyUp(unsigned int);

	bool isKeyPressed(unsigned int);
	bool isKeyDown(unsigned int);

private:
	bool keysPressed[256];
	bool keysDown[256];
};

#endif // INPUT_H
