#ifndef LEVELSCENE_H
#define LEVELSCENE_H

#include <vector>
#include "Scene.h"
#include "BallModel.h"
#include "BrickModel.h"

class LevelScene : public Scene {
    BallModel* ball;
    std::vector<BrickModel*> bricks;

    BrickModel* player;
    BrickModel* coopPlayer;

    bool moveLeft;
    bool moveRight;

public:
    LevelScene();
    ~LevelScene();

    void score();
    void hurt();

    void onEvent(Event event, std::string* data) override;
    void input(Input input) override;
    void update(float deltaTime) override;
};

#endif // LEVELSCENE_H
