#ifndef LOCK_H
#define LOCK_H

#include <windows.h>

class Lock {
private:
    CRITICAL_SECTION criticalSection;
    DWORD ret;
    volatile bool locked;

public:
    Lock();
    ~Lock();
    void enter();
    void leave();
    bool isLocked();
};

#endif // LOCK_H
