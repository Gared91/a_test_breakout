#ifndef MODEL_H
#define MODEL_H

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

class GL;

class Model {
protected:

    struct Vertex {
        glm::vec3 position;
        glm::vec2 texture;
        glm::vec4 color;
    };

    glm::vec3 position;
    glm::vec4 color;
    float scale;

    bool isNeedUpdate;

	int vertexCount, indexCount;
	unsigned int vertexArrayId, vertexBufferId, indexBufferId;
	void updateGL(GL gl);

public:
    Model();
    ~Model();

    bool visible;

    void setPosition(float x, float y);
    void setPosition(float x, float y, float z);
    void setScale(float s);
    float getX();
    float getY();

    virtual void getVertices(Vertex* vertices) = 0;
    virtual void getIndices(unsigned int* indices) = 0;

    void initialize(GL gl);
    void render(GL gl);
    void shutdown(GL gl);
};

#endif // MODEL_H
