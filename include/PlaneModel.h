#ifndef PLANEMODEL_H
#define PLANEMODEL_H

#include <Model.h>


class PlaneModel : public Model {
    float sizeW, sizeH;
    float u, v, w, h;

public:
    PlaneModel(float texU, float texV, float texW, float texH);
    PlaneModel(float sW, float sH, float texU, float texV, float texW, float texH);
    ~PlaneModel();

    void getVertices(Vertex* vertices) override;
    void getIndices(unsigned int* indices) override;
};

#endif // PLANEMODEL_H
