#ifndef SCENE_H
#define SCENE_H

#include <vector>
#include <string>
#include "Model.h"
#include "Event.h"

class Window;
class Input;
class GL;

class Scene {
private:
    std::vector<Model*> models;

protected:
    float r;
    float g;
    float b;

    Window *window;
	void changeScene(Scene *newScene);
	void addModel(Model* model);

public:
    Scene();
    virtual ~Scene();
    virtual void initialize(Window *window);
    virtual void initializeGL(GL gl);
    virtual void onEvent(Event event, std::string* data);
    virtual void input(Input input);
    virtual void update(float deltaTime);
    virtual void render(GL gl);
    virtual void shutdownGL(GL gl);
    virtual void finish();
};

#endif // SCENE_H
