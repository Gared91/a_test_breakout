#ifndef SERVER_H
#define SERVER_H

#include "Thread.h"
#include "Event.h"
#include <functional>
#include <string>
#include <vector>
#include <queue>
#include "Lock.h"

class ServerClientSendThread: public Thread {
    int iResult;
    std::queue<std::string*> dataToSend;
    Lock lock;
public:
    SOCKET socket;

    void sendData(std::string* data);
    void* run() override;
};

class ServerClientThread: public Thread {
    ServerClientSendThread* sendThread;
public:
    SOCKET socket;
    int iResult;
    int iSendResult;
    std::function<void(Event,std::string*)> serverCallback;
    void sendData(std::string* data);
    void* run() override;
};

class Server {

    SOCKET listenSocket;
    WSADATA wsaData;
    int iResult;
    struct addrinfo *result;

    std::vector<ServerClientThread*> serverClients;

public:
    std::function<void(Event,std::string*)> serverCallback;

    Server();
    ~Server();
    void start();
    void sendData(std::string* data);

private:
    void acceptNewClient(SOCKET socket);
    void listenForSockets();
    void clean(SOCKET socket);
};

#endif // SERVER_H
