#ifndef SERVERTHREAD_H
#define SERVERTHREAD_H

#include <Thread.h>
#include <Server.h>

class ServerThread: public Thread {
    Server* server;
public:
	ServerThread(Server* SERVER);
	void* run() override;
};

#endif // SERVERTHREAD_H
