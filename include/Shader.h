#ifndef SHADER_H
#define SHADER_H

class GL;

class Shader {
private:
	unsigned int vertexShader;
	unsigned int fragmentShader;
	unsigned int shaderProgram;

public:
    Shader();
    ~Shader();

    bool initialize(GL gl);
    bool initialize(GL gl, const char* vertexShaderStr, const char* fragmentShaderStr);
	void shutdown(GL gl);
	void setShader(GL gl);
	bool setShaderParameters(GL gl, float* worldMatrix, float* viewMatrix, float* projectionMatrix, int textureUnit);
};

#endif // SHADER_H
