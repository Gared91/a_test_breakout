#ifndef SIMPLEBUFFER_H
#define SIMPLEBUFFER_H

#include <string>

class SimpleBuffer {
private:
    char* buffer;
    unsigned int pos;
    unsigned int limit;

public:
    SimpleBuffer();
    ~SimpleBuffer();

    void flush();

    void putInt(int i);
    int getInt();
    void putFloat(float f);
    float getFloat();
    void setStr(std::string* s);
    std::string* getStr();
};

#endif // SIMPLEBUFFER_H
