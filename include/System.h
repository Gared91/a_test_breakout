#ifndef SYSTEM_H
#define SYSTEM_H


class System {
public:
    System();
    ~System();

	ATOM registerClass(HINSTANCE hInstance);
};

#endif // SYSTEM_H
