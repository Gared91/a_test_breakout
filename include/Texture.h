#ifndef TEXTURE_H
#define TEXTURE_H

#include <stdio.h>

class GL;

class Texture {
private:
	struct TargaHeader {
		unsigned char data1[12];
		unsigned short width;
		unsigned short height;
		unsigned char bpp;
		unsigned char data2;
	};

public:
	Texture();
	~Texture();

	bool initialize(GL, char*, unsigned int, bool);
	void bind();
	void shutdown();

private:
	bool loadTarga(GL, char*, unsigned int, bool);

private:
	bool loaded;
	unsigned int textureID;
};

#endif // TEXTURE_H
