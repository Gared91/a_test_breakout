#ifndef TITLESCENE_H
#define TITLESCENE_H

#include <Scene.h>
#include "PlaneModel.h"

class TitleScene : public Scene {
    float rMod;
    float gMod;
    float bMod;

    PlaneModel* hostPlane;
    PlaneModel* connectPlane;

    bool isWaitForNetwork;

    bool hostSelected;
    float scale;
    float scaleMod;

public:
    TitleScene();
    virtual ~TitleScene();

    void onEvent(Event event, std::string* data) override;
    void input(Input input) override;
    void update(float deltaTime) override;
};

#endif // TITLESCENE_H
