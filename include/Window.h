#ifndef WINDOW_H
#define WINDOW_H

#include "GL.h"
#include "Shader.h"
#include "Scene.h"
#include "Input.h"
#include "Camera.h"
#include "Texture.h"
#include "ServerThread.h"
#include "ClientThread.h"
#include "Lock.h"
#include <queue>

class Window {
private:
    bool isFullscreen;
    Shader shader;
    Input input;
    GL gl;
    Scene *scene;
    Texture texture;
    Camera camera;

    Server server;
    Client client;
    ServerThread* serverThread;
    ClientThread* clientThread;

    Lock lock;

    std::queue<std::pair<Event, std::string*>> networkData;

public:
	LPTSTR windowClass;
	HGLRC RC;
	HDC	DC;
	HWND WND;
	DWORD style;

	struct Config {
		int width;
		int	height;
		int posX;
		int posY;
		bool windowed;
	} config;

	void clearColor(float r, float g, float b);

	Window();
	~Window();
	int create(HINSTANCE hInstance, int nCmdShow);
	ATOM registerClass(HINSTANCE hInstance);
	void adjustSize();
	void center();
	void changeScene(Scene *newScene);
	void processInput();
	void update(float deltaTime);
	void render();
	void swapBuffers();
	void destroy();

	bool isHost();
    void startServer();
    void startClient();
    void sendData(std::string* data);

	LRESULT CALLBACK windowProcedure(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	static void showInfo(LPCSTR message);
	static void showError(LPCSTR message);
	static LRESULT CALLBACK WindowProcedure(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
private:

};

#endif
