#include "BallModel.h"
#include <cmath>

BallModel::BallModel() {

    color.r = 1.f;
    color.g = 1.f;
    color.b = 1.f;
    color.a = 1.f;
    sectorCount = 18.f;
    stackCount = 9.f;
    radius = 0.2f;
    velocity = {0.f, 0.f};

    const float PI = acos(-1);
    float x, y, z, xy, u, v;

    float sectorStep = 2 * PI / sectorCount;
    float stackStep = PI / stackCount;
    float sectorAngle, stackAngle;

    for(int i = 0; i <= stackCount; ++i) {
        stackAngle = PI / 2 - i * stackStep;
        xy = radius * cosf(stackAngle);
        z = radius * sinf(stackAngle);

        for(int j = 0; j <= sectorCount; ++j) {
            sectorAngle = j * sectorStep;

            x = xy * cosf(sectorAngle);
            y = xy * sinf(sectorAngle);
            u = (i % 2 == 0) ? 0.5f : 1.f;
            v = (j % 2 == 0) ? 0.5f : 1.f;
            verticesVector.push_back({ {x, y, z}, {u, v}, color });
        }
    }

    unsigned int k1, k2;
    for(int i = 0; i < stackCount; ++i) {
        k1 = i * (sectorCount + 1);
        k2 = k1 + sectorCount + 1;

        for(int j = 0; j < sectorCount; ++j, ++k1, ++k2) {
            if(i != 0) {
                indicesVector.push_back(k1);
                indicesVector.push_back(k2);
                indicesVector.push_back(k1 + 1);
            }
            if(i != (stackCount-1)) {
                indicesVector.push_back(k1 + 1);
                indicesVector.push_back(k2);
                indicesVector.push_back(k2 + 1);
            }
        }
    }

    vertexCount = verticesVector.size();
    indexCount = indicesVector.size();
}

BallModel::~BallModel() {
}

void BallModel::update(float deltaTime) {

    position.x += velocity.x * deltaTime;
    position.y += velocity.y * deltaTime;
    isNeedUpdate = true;
}

void BallModel::setVelocity(float x, float y) {

    velocity.x = x;
    velocity.y = y;
}

glm::vec2 BallModel::getVelocity() {
    return velocity;
}

void BallModel::getVertices(Vertex* vertices) {

    for (int i = 0; i < vertexCount; ++i) {
        vertices[i] = verticesVector.at(i);
    }
}

void BallModel::getIndices(unsigned int* indices) {

    for (int i = 0; i < indexCount; ++i) {
        indices[i] = indicesVector.at(i);
    }
}
