#include "BrickModel.h"

BrickModel::BrickModel() {

    vertexCount = 8;
    indexCount = 36;
    scale = 0.95f;
    color.r = 1.f;
    color.g = 1.f;
    color.b = 1.f;
    color.a = 1.f;
}

BrickModel::~BrickModel() {
}

void BrickModel::getVertices(Vertex* vertices) {

	vertices[0] = { {-1.f, -.5f, -.5f}, {1.f, 0.5f}, color };
	vertices[1] = { {1.f, -.5f, -.5f}, {0.5f, 0.5f}, color };
	vertices[2] = { {1.f, .5f, -.5f}, {0.5f, 0.f}, color };
	vertices[3] = { {-1.f, .5f, -.5f}, {1.f, 0.f}, color };
	vertices[4] = { {-1.f, -.5f, .5f}, {1.f, 0.5f}, color };
	vertices[5] = { {1.f, -.5f, .5f}, {0.5f, 0.5f}, color };
	vertices[6] = { {1.f, .5f, .5f}, {0.5f, 0.f}, color };
	vertices[7] = { {-1.f, .5f, .5f}, {1.f, 0.f}, color };
}

void BrickModel::getIndices(unsigned int* indices) {

    int tempIndices[] = {
        0, 2, 1, 2, 0, 3,//front
        3, 6, 2, 6, 3, 7,//top
        0, 7, 3, 7, 0, 4,//left
        1, 6, 5, 6, 1, 2,//right
        5, 7, 4, 7, 5, 6,//back
        4, 1, 5, 1, 4, 0//bottom
    };
    for (int i = 0; i < indexCount; ++i) {
        indices[i] = tempIndices[i];
    }
}
