#include "Camera.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

Camera::Camera() {
    position.x = 0.0f;
    position.y = 0.0f;
    position.z = -12.0f;
    origin.x = 0.0f;
    origin.y = 0.0f;
    origin.z = 0.0f;
}

Camera::~Camera() {
}

void Camera::setPosition(float x, float y) {
    origin.x = position.x = x;
    origin.y = position.y = y;
}

void Camera::move(float x, float y) {
    position.x += x;
    position.y += y;
    origin.x = position.x;
    origin.y = position.y;
}

void Camera::getPVMMatrix(float* worldMatrix, float* viewMatrix, float* projectionMatrix) {

    glm::mat4 projection = glm::perspective(glm::radians(45.0f), 16.0f / 9.0f, 0.1f, 100.0f);

    glm::mat4 view = glm::lookAt(position, origin, glm::vec3(0,1,0));

    glm::mat4 model = glm::mat4(1.0f);

    const float *projectionSource = (const float*)glm::value_ptr(projection);
    for (int i = 0; i < 16; ++i) projectionMatrix[i] = projectionSource[i];

    const float *viewSource = (const float*)glm::value_ptr(view);
    for (int i = 0; i < 16; ++i) viewMatrix[i] = viewSource[i];

    const float *modelSource = (const float*)glm::value_ptr(model);
    for (int i = 0; i < 16; ++i) worldMatrix[i] = modelSource[i];
}
