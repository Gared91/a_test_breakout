#include "Client.h"

#define WIN32_LEAN_AND_MEAN
#define _WIN32_WINNT 0x501
#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

void* ClientSendThread::run() {

    while (true) {
        if (dataToSend.size() > 0) {
            lock.enter();
            std::string* dataStr = dataToSend.front();
            dataToSend.pop();
            lock.leave();
            const char* data = dataStr->data();
            iResult = send(socket, data, (int) dataStr->size(), 0);
            if (iResult == SOCKET_ERROR) {
                closesocket(socket);
                return 0;
            }
            delete dataStr;
        }
    }
    iResult = shutdown(socket, SD_SEND);
    if (iResult == SOCKET_ERROR) {
        closesocket(socket);
        return 0;
    }
    return 0;
}

void ClientSendThread::sendData(std::string* data) {

    lock.enter();
    dataToSend.push(data);
    lock.leave();
}

Client::Client() {

    sendThread = nullptr;
}

Client::~Client() {
}

void Client::start() {

    connectSocket = INVALID_SOCKET;
    struct addrinfo *result = NULL, *ptr = NULL, hints;
    const char *sendbuf = "this is a test";
    char recvbuf[DEFAULT_BUFLEN];
    int recvbuflen = DEFAULT_BUFLEN;

    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        clientCallback(ClientStartError, nullptr);
        return;
    }

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    iResult = getaddrinfo("localhost", DEFAULT_PORT, &hints, &result);
    if (iResult != 0) {
        WSACleanup();
        clientCallback(ClientStartError, nullptr);
        return;
    }

    for(ptr = result; ptr != NULL; ptr = ptr->ai_next) {

        connectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
        if (connectSocket == INVALID_SOCKET) {
            WSACleanup();
            clientCallback(ClientStartError, nullptr);
            return;
        }
        iResult = connect(connectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(connectSocket);
            connectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }
    freeaddrinfo(result);

    if (connectSocket == INVALID_SOCKET) {
        WSACleanup();
        clientCallback(ClientStartError, nullptr);
        return;
    }

    clientCallback(ClientStarted, nullptr);

    sendThread = new ClientSendThread();
    sendThread->socket = connectSocket;
    sendThread->start();
    do {
        iResult = recv(connectSocket, recvbuf, recvbuflen, 0);
        if (iResult > 0) {
            clientCallback(ClientData, new std::string(recvbuf, iResult));
        }
    } while(iResult > 0);

    closesocket(connectSocket);
    WSACleanup();
}

void Client::sendData(std::string* data) {

    if (sendThread) sendThread->sendData(data);
}
