#include "ClientThread.h"

ClientThread::ClientThread(Client* CLIENT) : client(CLIENT) {
}

void* ClientThread::run() {
    client->start();
    return reinterpret_cast<void*>(0);
}
