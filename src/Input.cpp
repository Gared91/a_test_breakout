#include "Input.h"

Input::Input() {

    for(int i=0; i<256; i++) {
		keysPressed[i] = false;
		keysDown[i] = false;
	}
}

Input::~Input() {
}

void Input::clear() {

	for(int i=0; i<256; i++) {
		keysPressed[i] = false;
	}
}

void Input::keyDown(unsigned int input) {

	keysPressed[input] = true;
	keysDown[input] = true;
}

void Input::keyUp(unsigned int input) {

	keysDown[input] = false;
}

bool Input::isKeyPressed(unsigned int key) {

	return keysPressed[key];
}

bool Input::isKeyDown(unsigned int key) {

	return keysDown[key];
}
