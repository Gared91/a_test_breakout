#include "LevelScene.h"
#include "TitleScene.h"
#include "Window.h"
#include "PlaneModel.h"
#include "SimpleBuffer.h"
#include <cmath>

LevelScene::LevelScene() {
    r = 0.5f;
    g = 0.5f;
    b = 0.5f;

    PlaneModel* board = new PlaneModel(10.f, 10.f, 0.f, 0.f, 0.5f, 0.5f);
    board->setPosition(0.f, 0.f, 1.f);
    addModel(board);

    for (int y = 0; y < 4; ++y) {
        for (int x = 0; x < 5; ++x) {
            BrickModel* brick = new BrickModel();
            brick->setPosition(-4.f + (x * 2.f), 4.5f - y);
            bricks.push_back(brick);
            addModel(brick);
        }
    }
    ball = new BallModel();
    ball->setPosition(0.f, -1.f);
    addModel(ball);
    player = new BrickModel();
    player->setPosition(0.f, -4.5f);
    addModel(player);
    coopPlayer = new BrickModel();
    coopPlayer->setPosition(0.f, -4.5f);
    addModel(coopPlayer);
}

LevelScene::~LevelScene() {
}

void LevelScene::score() {

    r = 0.f;
    g = 0.f;
    b = 1.f;
}

void LevelScene::hurt() {

    r = 1.f;
    g = 0.f;
    b = 0.f;
    if (window && window->isHost()) {
        SimpleBuffer buffer;
        buffer.putInt(CmdHitFlor);
        window->sendData(buffer.getStr());
    }
}

void LevelScene::onEvent(Event event, std::string* data) {

    switch (event) {
        case ServerNewClient: {
            ball->setVelocity(2.75f, 2.75f);
            break;
        }
        case ClientData: {
            SimpleBuffer buffer;
            buffer.setStr(data);

            int cmd = buffer.getInt();
            switch (cmd) {
                case CmdBallMove: {
                    float x = buffer.getFloat();
                    float y = buffer.getFloat();
                    ball->setPosition(x, y);
                    break;
                }
                case CmdPlayerMove: {
                    float x = buffer.getFloat();
                    float y = buffer.getFloat();
                    coopPlayer->setPosition(x, y);
                    break;
                }
                case CmdHideBrick: {
                    int index = buffer.getInt();
                    bricks.at(index)->visible = false;
                    score();
                    break;
                }
            }
            break;
        }
        case ServerData: {
            SimpleBuffer buffer;
            buffer.setStr(data);

            int cmd = buffer.getInt();
            switch (cmd) {
                case CmdBallMove: {
                    float x = buffer.getFloat();
                    float y = buffer.getFloat();
                    ball->setPosition(x, y);
                    break;
                }
                case CmdPlayerMove: {
                    float x = buffer.getFloat();
                    float y = buffer.getFloat();
                    coopPlayer->setPosition(x, y);
                    break;
                }
            }
            break;
        }
    }
}

void LevelScene::input(Input input) {

    moveLeft = input.isKeyDown(VK_LEFT);
    moveRight = input.isKeyDown(VK_RIGHT);
}

bool boxCircleColision(float bX, float bY, float bW, float bH, float cX, float cY, float cR) {

}

bool circleRect(float cx, float cy, float radius, float rx, float ry, float rw, float rh) {

  float testX = cx;
  float testY = cy;

  if (cx < rx)         testX = rx;
  else if (cx > rx+rw) testX = rx+rw;
  if (cy < ry)         testY = ry;
  else if (cy > ry+rh) testY = ry+rh;

  float distX = cx-testX;
  float distY = cy-testY;
  float distance = sqrt( (distX*distX) + (distY*distY) );

  if (distance <= radius) {
    return true;
  }
  return false;
}

bool brickBall(BrickModel* brick, BallModel* ball) {
    if (brick->visible && circleRect(ball->getX(), ball->getY(), 0.2f, brick->getX() - 1.f, brick->getY() - .5f, 2.f, 1.f)) {
        glm::vec2 v = ball->getVelocity();
        ball->setVelocity(v.x, -v.y);
    }
}

void LevelScene::update(float deltaTime) {

    if (abs(r - 0.5f) > 0.01f) {
        r += abs(r - 0.5f) * ((r > 0.5f) ? -2.f : 2.f) * deltaTime;
    } else r = 0.5f;

    if (abs(g - 0.5f) > 0.01f) {
        g += abs(g - 0.5f) * ((g > 0.5f) ? -2.f : 2.f) * deltaTime;
    } else g = 0.5f;

    if (abs(b - 0.5f) > 0.01f) {
        b += abs(b - 0.5f) * ((b > 0.5f) ? -2.f : 2.f) * deltaTime;
    } else b = 0.5f;

    ball->update(deltaTime);
    if (ball->getX() - 0.2f < -5.f || 5.f < ball->getX() + 0.2f) {
        glm::vec2 v = ball->getVelocity();
        ball->setVelocity(-v.x, v.y);
    }
    if (ball->getY() - 0.2f < -5.f) {
        glm::vec2 v = ball->getVelocity();
        ball->setVelocity(v.x, -v.y);

        hurt();
    }
    if (5.f < ball->getY() + 0.2f) {
        glm::vec2 v = ball->getVelocity();
        ball->setVelocity(v.x, -v.y);
    }
    for (int i = 0; i < bricks.size(); ++i) {
        BrickModel* brick = bricks.at(i);
        if (brickBall(brick, ball)) {
            brick->visible = false;
            score();
            if (window && window->isHost()) {
                SimpleBuffer buffer;
                buffer.putInt(CmdHideBrick);
                buffer.putInt(i);
                window->sendData(buffer.getStr());
            }
        }
    }
    brickBall(player, ball);
    brickBall(coopPlayer, ball);

    if (moveLeft != moveRight) {
        float x = player->getX();
        if (moveLeft) x += 2.f * deltaTime;
        if (moveRight) x -= 2.f * deltaTime;
        player->setPosition(x, player->getY());

        SimpleBuffer buffer;
        buffer.putInt(CmdPlayerMove);
        buffer.putFloat(player->getX());
        buffer.putFloat(player->getY());
        window->sendData(buffer.getStr());
    }

    if (window && window->isHost()) {
        SimpleBuffer buffer;
        buffer.putInt(CmdBallMove);
        buffer.putFloat(ball->getX());
        buffer.putFloat(ball->getY());
        window->sendData(buffer.getStr());
    }
}
