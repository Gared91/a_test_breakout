#include "Lock.h"

Lock::Lock() {

    locked = false;
    ret = InitializeCriticalSectionAndSpinCount(&criticalSection, 0x80000400);
}

Lock::~Lock() {

    DeleteCriticalSection(&criticalSection);
}

void Lock::enter() {

    EnterCriticalSection(&criticalSection);
    locked = true;
}

void Lock::leave() {

    LeaveCriticalSection(&criticalSection);
    locked = false;
}

bool Lock::isLocked() {

    return locked;
}
