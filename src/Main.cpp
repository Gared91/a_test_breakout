#include "Main.h"
#include "Window.h"

Window window;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {

    if (window.create(hInstance, nCmdShow) != 0) PostQuitMessage(1);

	long currMillisec = 0;
	long lastMillisec = timeSinceEpochMillisec();

	MSG msg;
	bool active = true;
	while (active) {
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				active = false;
			}
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		window.processInput();
		currMillisec = timeSinceEpochMillisec();
		window.update((currMillisec - lastMillisec) / 1000.0f);
		lastMillisec = timeSinceEpochMillisec();
		window.render();
		window.swapBuffers();
	}

	window.destroy();

	return msg.wParam;
}
