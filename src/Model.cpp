#include "Model.h"
#include "GL.h"

Model::Model() {

    position = { 0.0f, 0.0f, 0.0f };
    scale = 1.f;
    visible = true;
    isNeedUpdate = true;
    vertexArrayId = vertexBufferId = indexBufferId = 0;
}

Model::~Model() {
}

void Model::setPosition(float x, float y) {

    if (position.x != x || position.y != y) {
        isNeedUpdate = true;
    }
    position.x = x;
    position.y = y;
}

void Model::setPosition(float x, float y, float z) {

    if (position.x != x || position.y != y || position.z != z) {
        isNeedUpdate = true;
    }
    position.x = x;
    position.y = y;
    position.z = z;
}

void Model::setScale(float s) {

    if (scale != s) {
        isNeedUpdate = true;
    }
    scale = s;
}

float Model::getX() {

    return position.x;
}

float Model::getY() {

    return position.y;
}

void Model::initialize(GL gl) {

	gl.glGenVertexArrays(1, &vertexArrayId);
	gl.glGenBuffers(1, &vertexBufferId);
    updateGL(gl);
}

void Model::updateGL(GL gl) {

    Vertex* vertices = new Vertex[vertexCount];
    getVertices(vertices);
    for (int i = 0; i < vertexCount; ++i) {
        vertices[i].position.x = vertices[i].position.x * scale + position.x;
        vertices[i].position.y = vertices[i].position.y * scale + position.y;
        vertices[i].position.z = vertices[i].position.z * scale + position.z;
    }
    unsigned int* indices = new unsigned int[indexCount];
	getIndices(indices);

	gl.glBindVertexArray(vertexArrayId);
	gl.glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
	gl.glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(Vertex), vertices, GL_STATIC_DRAW);

	gl.glEnableVertexAttribArray(0);// Vertex position.
	gl.glEnableVertexAttribArray(1);// Vertex texture.
	gl.glEnableVertexAttribArray(2);// Vertex color.

	gl.glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
	gl.glVertexAttribPointer(0, 3, GL_FLOAT, false, sizeof(Vertex), 0);

	gl.glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
	gl.glVertexAttribPointer(1, 2, GL_FLOAT, false, sizeof(Vertex), (unsigned char*)NULL + (3 * sizeof(float)));

	gl.glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
	gl.glVertexAttribPointer(2, 4, GL_FLOAT, false, sizeof(Vertex), (unsigned char*)NULL + (5 * sizeof(float)));

	gl.glGenBuffers(1, &indexBufferId);

	gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferId);
	gl.glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexCount* sizeof(unsigned int), indices, GL_STATIC_DRAW);

	delete [] vertices;
	delete [] indices;
	isNeedUpdate = false;
}

void Model::render(GL gl) {

    if (visible) {
        if (isNeedUpdate) updateGL(gl);
        gl.glBindVertexArray(vertexArrayId);
        glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
    }
}

void Model::shutdown(GL gl) {

	gl.glDisableVertexAttribArray(0);
	gl.glDisableVertexAttribArray(1);

	gl.glBindBuffer(GL_ARRAY_BUFFER, 0);
	gl.glDeleteBuffers(1, &vertexBufferId);

	gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	gl.glDeleteBuffers(1, &indexBufferId);

	gl.glBindVertexArray(0);
	gl.glDeleteVertexArrays(1, &vertexArrayId);
}
