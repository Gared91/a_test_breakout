#include "PlaneModel.h"

PlaneModel::PlaneModel(float texU, float texV, float texW, float texH) {

    sizeW = 1.f;
    sizeH = 1.f;
    u = texU;
    v = texV;
    w = texW;
    h = texH;

    vertexCount = 4;
    indexCount = 6;
    color.r = 1.f;
    color.g = 1.f;
    color.b = 1.f;
    color.a = 1.f;
}

PlaneModel::PlaneModel(float sW, float sH, float texU, float texV, float texW, float texH) {

    sizeW = sW;
    sizeH = sH;
    u = texU;
    v = texV;
    w = texW;
    h = texH;

    vertexCount = 4;
    indexCount = 6;
    color.r = 1.f;
    color.g = 1.f;
    color.b = 1.f;
    color.a = 1.f;
}

PlaneModel::~PlaneModel() {
}

void PlaneModel::getVertices(Vertex* vertices) {

	vertices[0] = { {sizeW * -.5f, sizeH * -.5f, 0.f}, {u + w, v + h}, color };
	vertices[1] = { {sizeW * .5f, sizeH * -.5f, 0.f}, {u, v + h}, color };
	vertices[2] = { {sizeW * .5f, sizeH * .5f, 0.f}, {u, v}, color };
	vertices[3] = { {sizeW * -.5f, sizeH * .5f, 0.f}, {u + w, v}, color };
}

void PlaneModel::getIndices(unsigned int* indices) {

    int tempIndices[] = {0, 2, 1, 2, 0, 3};
    for (int i = 0; i < indexCount; ++i) {
        indices[i] = tempIndices[i];
    }
}
