#include "Scene.h"
#include "Window.h"

Scene::Scene() {
    this->window = nullptr;
}

Scene::~Scene() {

}

void Scene::changeScene(Scene *newScene) {

    if (window) window->changeScene(newScene);
}

void Scene::addModel(Model* model) {

    models.push_back(model);
}

void Scene::initialize(Window *window) {

    this->window = window;
}

void Scene::initializeGL(GL gl) {

    for (int i = 0; i < models.size(); ++i) {
        models.at(i)->initialize(gl);
    }
}

void Scene::onEvent(Event event, std::string* data) {
}

void Scene::input(Input input) {
}

void Scene::update(float deltaTime) {
}

void Scene::render(GL gl) {

    if (window) window->clearColor(r, g, b);
    for (int i = 0; i < models.size(); ++i) {
        models.at(i)->render(gl);
    }
}

void Scene::shutdownGL(GL gl) {

    for (int i = 0; i < models.size(); ++i) {
        models.at(i)->shutdown(gl);
    }
}

void Scene::finish() {

    for (int i = 0; i < models.size(); ++i) {
        delete models.at(i);
    }
    this->window = nullptr;
}
