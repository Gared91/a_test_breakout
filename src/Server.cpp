#include "Server.h"

#define WIN32_LEAN_AND_MEAN
#define _WIN32_WINNT 0x501
#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

void* ServerClientSendThread::run() {

    while (true) {
        if (dataToSend.size() > 0) {
            lock.enter();
            std::string* dataStr = dataToSend.front();
            dataToSend.pop();
            lock.leave();
            const char* data = dataStr->data();
            iResult = send(socket, data, (int) dataStr->size(), 0);
            if (iResult == SOCKET_ERROR) {
                closesocket(socket);
                return 0;
            }
            delete dataStr;
        }
    }
    iResult = shutdown(socket, SD_SEND);
    if (iResult == SOCKET_ERROR) {
        closesocket(socket);
        return 0;
    }
    return 0;
}

void ServerClientSendThread::sendData(std::string* data) {

    lock.enter();
    dataToSend.push(data);
    lock.leave();
}

void ServerClientThread::sendData(std::string* data) {

    if (sendThread) sendThread->sendData(data);
}

void* ServerClientThread::run() {

    char recvbuf[DEFAULT_BUFLEN];
    int recvbuflen = DEFAULT_BUFLEN;

    sendThread = new ServerClientSendThread();
    sendThread->socket = socket;
    sendThread->start();

    do {
        iResult = recv(socket, recvbuf, recvbuflen, 0);
        if (iResult > 0) {
            serverCallback(ServerData, new std::string(recvbuf, iResult));
        } else {
            closesocket(socket);
            return 0;
        }

    } while (iResult > 0);

    iResult = shutdown(socket, SD_SEND);
    if (iResult == SOCKET_ERROR) {
        closesocket(socket);
        return 0;
    }

    closesocket(socket);
}

Server::Server() {
}

Server::~Server() {
}

void Server::start() {

    listenSocket = INVALID_SOCKET;

    struct addrinfo hints;
    result = NULL;

    char recvbuf[DEFAULT_BUFLEN];
    int recvbuflen = DEFAULT_BUFLEN;

    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        serverCallback(ServerStartError, nullptr);
        return;
    }

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;

    iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
    if (iResult != 0) {
        WSACleanup();
        serverCallback(ServerStartError, nullptr);
        return;
    }

    serverCallback(ServerStarted, nullptr);

    listenForSockets();
}

void Server::sendData(std::string* data) {

    for (int i = 0; i < serverClients.size(); ++i) {
        serverClients.at(i)->sendData(data);
    }
}

void Server::acceptNewClient(SOCKET socket) {

    ServerClientThread* clientThread = new ServerClientThread();
    clientThread->serverCallback = serverCallback;
    clientThread->socket = socket;
    clientThread->start();
    serverClients.push_back(clientThread);

    serverCallback(ServerNewClient, nullptr);
}

void Server::listenForSockets() {

    listenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (listenSocket == INVALID_SOCKET) {
        freeaddrinfo(result);
        WSACleanup();
        serverCallback(ServerStartError, nullptr);
        return;
    }

    iResult = bind(listenSocket, result->ai_addr, (int)result->ai_addrlen);
    if (iResult == SOCKET_ERROR) {
        freeaddrinfo(result);
        clean(listenSocket);
        serverCallback(ServerStartError, nullptr);
        return;
    }

    freeaddrinfo(result);

    iResult = listen(listenSocket, SOMAXCONN);
    if (iResult == SOCKET_ERROR) {
        clean(listenSocket);
        serverCallback(ServerStartError, nullptr);
        return;
    }

    SOCKET clientSocket;
    while (true) {
        clientSocket = accept(listenSocket, NULL, NULL);
        if (clientSocket == INVALID_SOCKET) {
            clean(listenSocket);
            serverCallback(ServerStartError, nullptr);
            return;
        } else {
            acceptNewClient(clientSocket);
        }
    }

    closesocket(listenSocket);
}

void Server::clean(SOCKET socket) {

    closesocket(socket);
    WSACleanup();
}
