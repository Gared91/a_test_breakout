#include "ServerThread.h"

ServerThread::ServerThread(Server* SERVER) : server(SERVER) {
}

void* ServerThread::run() {
    server->start();
    return reinterpret_cast<void*>(0);
}
