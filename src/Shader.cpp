#include "Shader.h"
#include "GL.h"
#include <string>

Shader::Shader() {
}

Shader::~Shader() {
}

bool Shader::initialize(GL gl) {

    std::string vertexShaderStr =
    "#version 400\n\
    \n\
    in vec3 inputPosition;\n\
    in vec2 inputTexCoord;\n\
    in vec4 inputColor;\n\
    \n\
    out vec2 texCoord;\n\
    out vec4 color;\n\
    \n\
    uniform mat4 worldMatrix;\n\
    uniform mat4 viewMatrix;\n\
    uniform mat4 projectionMatrix;\n\
    \n\
    void main(void) {\n\
        gl_Position = worldMatrix * vec4(inputPosition, 1.0f);\n\
        gl_Position = viewMatrix * gl_Position;\n\
        gl_Position = projectionMatrix * gl_Position;\n\
        \n\
        texCoord = inputTexCoord;\n\
        color = inputColor;\n\
    }";

    std::string fragmentShaderStr =
    "#version 400\n\
    \n\
    in vec2 texCoord;\n\
    in vec4 color;\n\
    \n\
    out vec4 outputColor;\n\
    \n\
    uniform sampler2D shaderTexture;\n\
    \n\
    void main(void) {\n\
        vec4 textureColor = texture(shaderTexture, texCoord);\n\
        outputColor = textureColor * color;\n\
    }";

    return initialize(gl, vertexShaderStr.c_str(), fragmentShaderStr.c_str());
}

void outputShaderErrorMessage(GL gl, unsigned int shaderId, std::string shaderFilename) {

	int logSize;
	char* infoLog;

	gl.glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &logSize);
	logSize++;

	infoLog = new char[logSize];
	gl.glGetShaderInfoLog(shaderId, logSize, NULL, infoLog);
	MessageBox(0, infoLog, shaderFilename.c_str(), MB_ICONERROR);
}

bool Shader::initialize(GL gl, const char* vertexShaderStr, const char* fragmentShaderStr) {

	int status;

	vertexShader = gl.glCreateShader(GL_VERTEX_SHADER);
	fragmentShader = gl.glCreateShader(GL_FRAGMENT_SHADER);

	gl.glShaderSource(vertexShader, 1, &vertexShaderStr, NULL);
	gl.glShaderSource(fragmentShader, 1, &fragmentShaderStr, NULL);

	gl.glCompileShader(vertexShader);
	gl.glCompileShader(fragmentShader);

	gl.glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);
	if(status != 1) {
		outputShaderErrorMessage(gl, vertexShader, "vertexShaderStr");
		return false;
	}

	gl.glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);
	if(status != 1) {
		outputShaderErrorMessage(gl, fragmentShader, "fragmentShaderStr");
		return false;
	}

	shaderProgram = gl.glCreateProgram();

	gl.glAttachShader(shaderProgram, vertexShader);
	gl.glAttachShader(shaderProgram, fragmentShader);

	gl.glBindAttribLocation(shaderProgram, 0, "inputPosition");
	gl.glBindAttribLocation(shaderProgram, 1, "inputTexCoord");
	gl.glBindAttribLocation(shaderProgram, 2, "inputColor");

	gl.glLinkProgram(shaderProgram);

	gl.glGetProgramiv(shaderProgram, GL_LINK_STATUS, &status);
	if(status != 1) return false;

	return true;
}

void Shader::shutdown(GL gl) {
	gl.glDetachShader(shaderProgram, vertexShader);
	gl.glDetachShader(shaderProgram, fragmentShader);

	gl.glDeleteShader(vertexShader);
	gl.glDeleteShader(fragmentShader);

	gl.glDeleteProgram(shaderProgram);
}

void Shader::setShader(GL gl) {

    gl.glUseProgram(shaderProgram);
}

bool Shader::setShaderParameters(GL gl, float* worldMatrix, float* viewMatrix, float* projectionMatrix, int textureUnit) {

    int location = gl.glGetUniformLocation(shaderProgram, "worldMatrix");
	if(location == -1) return false;

	gl.glUniformMatrix4fv(location, 1, false, worldMatrix);

	location = gl.glGetUniformLocation(shaderProgram, "viewMatrix");
	if(location == -1) return false;

	gl.glUniformMatrix4fv(location, 1, false, viewMatrix);

	location = gl.glGetUniformLocation(shaderProgram, "projectionMatrix");
	if(location == -1) return false;

	gl.glUniformMatrix4fv(location, 1, false, projectionMatrix);

	location = gl.glGetUniformLocation(shaderProgram, "shaderTexture");
	if(location == -1) return false;

	gl.glUniform1i(location, textureUnit);

	return true;
}
