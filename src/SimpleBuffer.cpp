#include "SimpleBuffer.h"
#include <cstring>

SimpleBuffer::SimpleBuffer() {

    pos = 0;
    limit = 128;
    buffer = new char[limit];
}

SimpleBuffer::~SimpleBuffer() {
    delete buffer;
}

void SimpleBuffer::flush() {
    pos = 0;
}

void SimpleBuffer::putInt(int i) {
    auto size = sizeof(int);
    memcpy(buffer + pos, &i, size);
    pos += size;
}

int SimpleBuffer::getInt() {
    int i = 0;
    auto size = sizeof(int);
    memcpy(&i, buffer + pos, size);
    pos += size;
    return i;
}

void SimpleBuffer::putFloat(float f) {
    auto size = sizeof(float);
    memcpy(buffer + pos, &f, size);
    pos += size;
}

float SimpleBuffer::getFloat() {
    float f = 0xff;
    auto size = sizeof(float);
    memcpy(&f, buffer + pos, size);
    pos += size;
    return f;
}

void SimpleBuffer::setStr(std::string* s) {
    const char* data = s->data();
    int size = s->size();
    for (int i = 0; i < size; ++i)
        buffer[i] = data[i];
    pos = 0;
}

std::string* SimpleBuffer::getStr() {
    return new std::string(buffer, pos);
}
