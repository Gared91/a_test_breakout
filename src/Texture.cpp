#include "Texture.h"
#include "GL.h"
#include <cerrno>
#include "Window.h"

Texture::Texture() {

    loaded = false;
}

Texture::~Texture() {
}

bool Texture::initialize(GL gl, char* filename, unsigned int textureUnit, bool wrap) {

    bool result;

	result = loadTarga(gl, filename, textureUnit, wrap);
	if(!result) {
		return false;
	}
	return true;
}

void Texture::bind() {

    glBindTexture(GL_TEXTURE_2D, textureID);
}

void Texture::shutdown() {

	if(loaded) {
		glDeleteTextures(1, &textureID);
		loaded = false;
	}
}

bool Texture::loadTarga(GL gl, char* filename, unsigned int textureUnit, bool wrap) {

	int error, width, height, bpp, imageSize;
	TargaHeader targaFileHeader;

	FILE* filePtr = fopen(filename, "rb");
	if (!filePtr) {
        char buffer[ 256 ];
        strerror_r( errno, buffer, 256 );
        Window::showInfo(buffer);
        return false;
	}

	unsigned int count = fread(&targaFileHeader, sizeof(TargaHeader), 1, filePtr);
	if(count != 1) {
        char buffer[ 256 ];
        strerror_r( errno, buffer, 256 );
        Window::showInfo(buffer);
		return false;
	}

	width = (int)targaFileHeader.width;
	height = (int)targaFileHeader.height;
	bpp = (int)targaFileHeader.bpp;

	if(bpp != 32) {
		return false;
	}

	imageSize = width * height * 4;

	unsigned char* targaImage = new unsigned char[imageSize];
	if(!targaImage) {
        char buffer[ 256 ];
        strerror_r( errno, buffer, 256 );
        Window::showInfo(buffer);
		return false;
	}

	count = fread(targaImage, 1, imageSize, filePtr);
	if(count != imageSize) {
        char buffer[ 256 ];
        strerror_r( errno, buffer, 256 );
        Window::showInfo(buffer);
		return false;
	}

	error = fclose(filePtr);
	if(error != 0) {
        char buffer[ 256 ];
        strerror_r( errno, buffer, 256 );
        Window::showInfo(buffer);
		return false;
	}

	gl.glActiveTexture(GL_TEXTURE0 + textureUnit);

	glGenTextures(1, &textureID);

	bind();

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, targaImage);

	if(wrap) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	} else {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	gl.glGenerateMipmap(GL_TEXTURE_2D);

	delete [] targaImage;
	targaImage = 0;

	loaded = true;

	return true;
}
