#include "TitleScene.h"
#include "LevelScene.h"
#include "Window.h"

TitleScene::TitleScene() {

    isWaitForNetwork = false;
    hostSelected = true;
    scale= 3.f;
    scaleMod = 2.f;

    r = 0.2f;
    rMod = 0.3f;
    g = 0.4f;
    gMod = 0.4f;
    b = 0.6f;
    bMod = 0.5f;

    hostPlane = new PlaneModel(1.f, 0.5f, 0.f, 0.75f, 0.5f, 0.25f);
    hostPlane->setPosition(2.f, -1.f);
    addModel(hostPlane);

    connectPlane = new PlaneModel(1.f, 0.5f, 0.f, 0.5f, 0.5f, 0.25f);
    connectPlane->setPosition(-2.f, -1.f);
    addModel(connectPlane);
}

TitleScene::~TitleScene() {
}

void TitleScene::onEvent(Event event, std::string* data) {

    switch (event) {
        case ServerStarted:
        case ClientStarted:
            window->changeScene(new LevelScene());
            break;
        case ClientStartError:
            Window::showInfo("ClientStartError");
            isWaitForNetwork = true;
            break;
        case ServerStartError:
            Window::showInfo("ServerStartError");
            isWaitForNetwork = true;
            break;
    }
}

void TitleScene::input(Input input) {

    if (!isWaitForNetwork && input.isKeyPressed(VK_RETURN)) {
        if (window) {
            if (hostSelected)
                window->startServer();
            else
                window->startClient();

        }
    }
    if (input.isKeyPressed(VK_LEFT) || input.isKeyPressed(VK_RIGHT)) {
        hostSelected = !hostSelected;
        scaleMod = 2.f;
        scale= 3.f;
    }
}

void TitleScene::update(float deltaTime) {

    r += deltaTime * rMod;
    if (r < 0.2f || 0.8f < r) rMod = rMod * -1.f;
    g += deltaTime * gMod;
    if (g < 0.2f || 0.8f < g) gMod = gMod * -1.f;
    b += deltaTime * bMod;
    if (b < 0.2f || 0.8f < b) bMod = bMod * -1.f;

    scale += deltaTime * scaleMod;
    if (scale < 2.5f || 3.5f < scale) scaleMod = scaleMod * -1.f;

    if (hostSelected) {
        hostPlane->setScale(scale);
        connectPlane->setScale(3.f);
    } else {
        hostPlane->setScale(3.f);
        connectPlane->setScale(scale);
    }
}
