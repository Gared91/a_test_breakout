#include "Window.h"
#include "TitleScene.h"
#include "GL.h"
#include <string>
#include <fstream>

Window::Window() {

    isFullscreen = false;
    scene = nullptr;
    config.width = 1024;
	config.height = 576;
	config.posX = CW_USEDEFAULT;
	config.posY = 0;
	config.windowed = true;
	style = WS_CAPTION | WS_SYSMENU | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

    server.serverCallback = [&](Event event, std::string* data) {
        if (scene) {
            lock.enter();
            networkData.push(std::pair<Event, std::string*>(event, data));
            lock.leave();
        }
    };
    client.clientCallback = [&](Event event, std::string* data) {
        if (scene) {
            lock.enter();
            networkData.push(std::pair<Event, std::string*>(event, data));
            lock.leave();
        }
    };
}

Window::~Window() {
}

void Window::showInfo(LPCSTR message) {

	MessageBox(0, message, "Info", MB_ICONINFORMATION);
}

void Window::showError(LPCSTR message) {

	MessageBox(0, message, "Error", MB_ICONERROR);
}

int Window::create(HINSTANCE hInstance, int nCmdShow) {

	windowClass = MAKEINTATOM(registerClass(hInstance));
	if (windowClass == 0) {
		showError("registerClass() failed.");
		return 1;
	}

	HWND fakeWND = CreateWindow(
		windowClass, "Fake Window",
		style,
		0, 0,
		1, 1,
		NULL, NULL,
		hInstance, NULL);

	HDC fakeDC = GetDC(fakeWND);

	PIXELFORMATDESCRIPTOR fakePFD;
	ZeroMemory(&fakePFD, sizeof(fakePFD));
	fakePFD.nSize = sizeof(fakePFD);
	fakePFD.nVersion = 1;
	fakePFD.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	fakePFD.iPixelType = PFD_TYPE_RGBA;
	fakePFD.cColorBits = 32;
	fakePFD.cAlphaBits = 8;
	fakePFD.cDepthBits = 24;

	const int fakePFDID = ChoosePixelFormat(fakeDC, &fakePFD);
	if (fakePFDID == 0) {
		showError("ChoosePixelFormat() failed.");
		return 1;
	}

	if (SetPixelFormat(fakeDC, fakePFDID, &fakePFD) == false) {
		showError("SetPixelFormat() failed.");
		return 1;
	}

	HGLRC fakeRC = wglCreateContext(fakeDC);

	if (fakeRC == 0) {
		showError("wglCreateContext() failed.");
		return 1;
	}

	if (wglMakeCurrent(fakeDC, fakeRC) == false) {
		showError("wglMakeCurrent() failed.");
		return 1;
	}

	if (!gl.loadExtensionList()) {
		showError("GL loadExtensionList() failed.");
		return 1;
	}

	if (config.windowed == true) {
		adjustSize();
		center();
	}

	WND = CreateWindow(
		windowClass, "OpenGL Window",
		style,
		config.posX, config.posY,
		config.width, config.height,
		NULL, NULL,
		hInstance, NULL);

	DC = GetDC(WND);

	const int pixelAttribs[] = {
		WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
		WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
		WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
		WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
		WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
		WGL_COLOR_BITS_ARB, 32,
		WGL_ALPHA_BITS_ARB, 8,
		WGL_DEPTH_BITS_ARB, 24,
		WGL_STENCIL_BITS_ARB, 8,
        WGL_SWAP_METHOD_ARB, WGL_SWAP_EXCHANGE_ARB,
		WGL_SAMPLE_BUFFERS_ARB, GL_TRUE,
		WGL_SAMPLES_ARB, 4,
		0
	};

	int pixelFormatID; UINT numFormats;
	const bool status = gl.wglChoosePixelFormatARB(DC, pixelAttribs, NULL, 1, &pixelFormatID, &numFormats);

	if (status == false || numFormats == 0) {
		showError("wglChoosePixelFormatARB() failed.");
		return 1;
	}

	PIXELFORMATDESCRIPTOR PFD;
	DescribePixelFormat(DC, pixelFormatID, sizeof(PFD), &PFD);
	SetPixelFormat(DC, pixelFormatID, &PFD);

	const int contextAttribs[] = {
		WGL_CONTEXT_MAJOR_VERSION_ARB, 4,
		WGL_CONTEXT_MINOR_VERSION_ARB, 0,
		WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
		0
	};

	RC = gl.wglCreateContextAttribsARB(DC, 0, contextAttribs);
	if (RC == NULL) {
		showError("wglCreateContextAttribsARB() failed.");
		return 1;
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(fakeRC);
	ReleaseDC(fakeWND, fakeDC);
	DestroyWindow(fakeWND);
	if (!wglMakeCurrent(DC, RC)) {
		showError("wglMakeCurrent() failed.");
		return 1;
	}

    SetWindowLongPtrW(WND, 0, reinterpret_cast<LONG_PTR>(this));
	SetWindowText(WND, reinterpret_cast<LPCSTR>(glGetString(GL_VERSION)));
	ShowWindow(WND, nCmdShow);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glFrontFace(GL_CW);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

    if (!shader.initialize(gl)) {
        return 1;
    }

    if (!texture.initialize(gl, "../../data/test.tga", 0, true)) {
        return 1;
    }

	changeScene(new TitleScene());

	return 0;
}

ATOM Window::registerClass(HINSTANCE hInstance) {

	WNDCLASSEX wcex;
	ZeroMemory(&wcex, sizeof(wcex));
	wcex.cbSize = sizeof(wcex);
	wcex.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wcex.lpfnWndProc = WindowProcedure;
	wcex.hInstance = hInstance;
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.lpszClassName = "Core";
	wcex.cbWndExtra = sizeof(Window*);

	return RegisterClassEx(&wcex);
}

void Window::adjustSize() {

	RECT rect = { 0, 0, config.width, config.height };
	AdjustWindowRect(&rect, style, false);
	config.width = rect.right - rect.left;
	config.height = rect.bottom - rect.top;
}

void Window::center() {

	RECT primaryDisplaySize;
	SystemParametersInfo(SPI_GETWORKAREA, 0, &primaryDisplaySize, 0);
	config.posX = (primaryDisplaySize.right - config.width) / 2;
	config.posY = (primaryDisplaySize.bottom - config.height) / 2;
}

void Window::changeScene(Scene *newScene) {

    if (scene) {
        scene->shutdownGL(gl);
        scene->finish();
    }
    scene = newScene;
    camera.setPosition(0.f, 0.f);
    if (scene) {
        scene->initialize(this);
        scene->initializeGL(gl);
    }
}

void Window::clearColor(float r, float g, float b) {

    glClearColor(r, g, b, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
}

void Window::processInput() {

    if (scene) {
        lock.enter();
        while (!networkData.empty()) {
            std::pair<Event, std::string*> eventData = networkData.front();
            networkData.pop();
            scene->onEvent(eventData.first, eventData.second);
        }
        lock.leave();
    }
    if (input.isKeyDown(VK_ESCAPE)) {
        PostQuitMessage(0);
    } else if (scene) {
        scene->input(input);
        input.clear();
    }
}

void Window::update(float deltaTime) {

    if (deltaTime > 0.017f) deltaTime = 0.017f;
    if (scene) scene->update(deltaTime);
}

void Window::render() {

    float worldMatrix[16];
	float viewMatrix[16];
	float projectionMatrix[16];
    camera.getPVMMatrix(worldMatrix, viewMatrix, projectionMatrix);

    shader.setShader(gl);
    shader.setShaderParameters(gl, worldMatrix, viewMatrix, projectionMatrix, 0);
    texture.bind();

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    if (scene) {
        scene->render(gl);
    }
}

void Window::swapBuffers() {

	SwapBuffers(DC);
}

void Window::destroy() {

	if (scene) {
        scene->shutdownGL(gl);
        scene->finish();
    }
    texture.shutdown();
    wglMakeCurrent(NULL, NULL);
	if (RC) wglDeleteContext(RC);
	if (DC) ReleaseDC(WND, DC);
	if (WND) DestroyWindow(WND);
}

bool Window::isHost() {
    return (serverThread != nullptr);
}

void Window::startServer() {

    if (serverThread) serverThread->join();
    serverThread = new ServerThread(&server);
    serverThread->start();
}

void Window::startClient() {

    if (clientThread) clientThread->join();
    clientThread = new ClientThread(&client);
    clientThread->start();
}

void Window::sendData(std::string* data) {

    if (serverThread) server.sendData(data);
    if (clientThread) client.sendData(data);
}

LRESULT CALLBACK Window::windowProcedure(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {

	switch (message) {
		case WM_KEYDOWN:
		    input.keyDown(wParam);
			break;
		case WM_KEYUP:
		    input.keyUp(wParam);
			break;
		case WM_CLOSE:
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

LRESULT CALLBACK Window::WindowProcedure(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {

    Window* pWnd = reinterpret_cast<Window*>(GetWindowLongPtrW(hWnd, 0));
    if (pWnd != NULL) {
        return pWnd->windowProcedure(hWnd, message, wParam, lParam);
    } else {
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
}
